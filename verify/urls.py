from django.urls import path

from verify.views import Dashboard, SendReportedNews, AdminDashboard, UpdateNews, InfoList

app_name = 'verify'

urlpatterns = [
    path('dashboard/', Dashboard.as_view(), name='dashboard'),
    path('invia_news/<int:pk>', SendReportedNews.as_view(), name='send-news'),
    path('dashboard_admin/', AdminDashboard.as_view(), name='dashboard-admin'),
    path('edit_news/<int:pk>', UpdateNews.as_view(), name='edit-news'),
    path('dashboard_info', InfoList.as_view(), name='dashboard-info')
]
