from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from verify.models import NewsVerified


class SendNewsForm (forms.ModelForm):

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.add_input(Submit('news_submit', 'Invia notizia'))
    helper.inputs[0].field_classes = 'btn btn-danger'

    class Meta:
        model = NewsVerified
        fields = ('resume', 'category', 'status')


class UpdateNewsForm(forms.ModelForm):

    helper = FormHelper()
    helper.add_input(Submit('news_submit', 'Pubblica notizia'))
    helper.inputs[0].field_classes = 'btn btn-danger'

    class Meta:
        model = NewsVerified
        fields = ('title', 'final_text', 'image', 'approved')
