from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from report.models import NewsReported, RequestedInfo
from verify.forms import SendNewsForm, UpdateNewsForm
from verify.models import NewsVerified


class SendNews (LoginRequiredMixin, SuccessMessageMixin, CreateView):

    model = NewsVerified
    form_class = SendNewsForm
    template_name = 'verify/verify_news.html'
    success_url = reverse_lazy('verify:dashboard')
    success_message = "notizia inviata correttamente all'amministratore"

    def form_valid(self, form):  # uso la pk della segnalazione per collegarla all'istanza della notizia
        pk = self.request.META.get("HTTP_REFERER")  # url della pagina attuale
        pk = pk.split('/')[-1]
        key = NewsReported.objects.get(pk=pk)
        form.instance.newsreported_ptr = key
        return super().form_valid(form)


class Dashboard (LoginRequiredMixin, ListView):

    model = NewsReported
    template_name = 'verify/dashboard.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return redirect('../dashboard_admin')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs['object_list'] = NewsReported.objects.order_by('-id')
        kwargs['pk_verified'] = NewsVerified.objects.values_list('newsreported_ptr', flat=True)
        return super(Dashboard, self).get_context_data(**kwargs)


class SendReportedNews (LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        view = ReportedNewsDetail.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = SendNews.as_view()
        return view(request, *args, **kwargs)


class ReportedNewsDetail (LoginRequiredMixin, DetailView):

    model = NewsReported
    template_name = 'verify/verify_news.html'
    success_url = reverse_lazy('verify:dashboard')

    def get_context_data(self, **kwargs):  # così il form sarà disponibile nei template
        context = super().get_context_data(**kwargs)
        context['form'] = SendNewsForm()
        return context


class AdminDashboard(LoginRequiredMixin, ListView):

    model = NewsVerified
    template_name = 'verify/dashboard_admin.html'

    def get_context_data(self, **kwargs):  # per l'ordinamento in base all'approvazione
        kwargs['object_list'] = NewsVerified.objects.order_by('-approved')
        return super(AdminDashboard, self).get_context_data(**kwargs)


class InfoList (LoginRequiredMixin, ListView):

    model = RequestedInfo
    template_name = 'verify/dashboard_info.html'


class UpdateNews(LoginRequiredMixin, SuccessMessageMixin, UpdateView):

    model = NewsVerified
    template_name = 'verify/edit_news.html'
    form_class = UpdateNewsForm
    success_url = reverse_lazy('verify:dashboard-admin')
    success_message = "notizia approvata correttamente"
