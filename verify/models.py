from django.db import models
from report.models import NewsReported


class NewsVerified (models.Model):

    resume = models.TextField()  # riassunto scritto dal verificatore. Non è necessariamente il testo finale
    final_text = models.TextField()
    image = models.ImageField(upload_to='static/img', null=True)
    title = models.CharField(max_length=70, null=True)
    newsreported_ptr = models.OneToOneField(NewsReported, on_delete=models.CASCADE)

    # notizia vera o falsa
    STATUS_CHOICES = [(True, 'Notizia vera'), (False, 'Notizia falsa')]
    status = models.BooleanField(choices=STATUS_CHOICES)

    # approvazione dell'amministratore
    WAITING = 'wait'
    ACCEPTED = 'ok'
    REJECTED = 'no'
    CHOICES = [(WAITING, 'da approvare'), (ACCEPTED, 'approvata'), (REJECTED, 'rifiutata')]
    approved = models.CharField(max_length=4, choices=CHOICES, default=WAITING)

    # categorie delle notizie
    CRONACA = 'cronaca'
    SPORT = 'sport'
    TECNOLOGIA = 'tecnologia/scienze'
    ECONOMIA = 'economia'
    CATEGORY_CHOICES = [(CRONACA, 'cronaca'), (SPORT, 'sport'), (TECNOLOGIA, 'tecnologia'), (ECONOMIA, 'economia')]
    category = models.CharField(max_length=20, choices=CATEGORY_CHOICES, default=CRONACA)

    class Meta:
        verbose_name_plural = 'NewsVerified'
