from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from verify.models import NewsVerified


class DashboardTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="utente1", is_superuser=True, password='passwordtest')

    def test_200_status_code(self):
        self.client.force_login(user=self.user)
        response = self.client.get(reverse('verify:dashboard-admin'))
        self.assertEqual(response.status_code, 200)

    # test HTML
    def test_dashboard_contains_username(self):
        self.client.force_login(user=self.user)
        response = self.client.get(reverse('verify:dashboard-admin'))
        self.assertContains(response, f"Benvenuto nella dashboard, {self.user.username} ")

    def test_table_contains_elements_of_database(self):
        self.client.force_login(user=self.user)
        response = self.client.get(reverse('verify:dashboard-admin'))
        for notizia in NewsVerified.objects.all():
            self.assertContains(response, f"<a href=\"/verify/invia_news/{notizia.pk}")

    def tearDown(self):
        del self.user
