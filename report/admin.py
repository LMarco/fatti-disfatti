from django.contrib import admin
from .models import RequestedInfo, NewsReported, Donation

# Register your models here.
admin.site.register(RequestedInfo)
admin.site.register(NewsReported)


@admin.register(Donation)
class DonationAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'amount')
    list_filter = ("amount", )
    search_fields = ('name', 'surname')
