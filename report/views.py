from django.db.models import Q
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView
from report.forms import ReportForm, InfoForm, DonationForm
from report.models import RequestedInfo, Donation
from verify.models import NewsVerified


class NewsAndInfoCreate(CreateView):

    model = RequestedInfo
    template_name = 'report/report_news.html'
    success_url = reverse_lazy('my_project:form-compiled')
    form_class = InfoForm

    def get_context_data(self, **kwargs):
        context = super(NewsAndInfoCreate, self).get_context_data(**kwargs)
        context['report_form'] = ReportForm()
        context['info_form'] = InfoForm()
        return context

    def post(self, request, *args, **kwargs):
        report_form = ReportForm(data=request.POST)
        info_form = InfoForm(data=request.POST)
        if 'report_submit' in request.POST:
            if report_form.is_bound and report_form.is_valid():
                report_form.save()
        if 'info_submit' in request.POST:
            if info_form.is_bound and info_form.is_valid():
                info_form.save()
        return redirect('form-compiled')


class DonationCreate(CreateView):

    model = Donation
    template_name = 'report/donate.html'
    success_url = reverse_lazy('report:donation-create')
    form_class = DonationForm

    def get_context_data(self, **kwargs):
        kwargs['object_list'] = Donation.objects.order_by('-id')
        return super(DonationCreate, self).get_context_data(**kwargs)


class ReadNews(DetailView):

    model = NewsVerified
    template_name = 'report/read_news.html'


class SearchResults(ListView):

    model = NewsVerified
    template_name = 'report/results.html'

    def get_queryset(self):
        query = self.request.GET.get("q")
        category = self.request.GET.get("category")
        if query:
            object_list = NewsVerified.objects.filter((Q(title__icontains=query) | Q(final_text__icontains=query)) &
                                                      Q(approved__exact='ok'))
        elif category:
            object_list = NewsVerified.objects.filter(Q(category__icontains=category) & Q(approved__exact='ok'))
        else:
            object_list = None
        return object_list


class Statistics(ListView):

    template_name = 'report/statistics.html'
    model = NewsVerified

    def get_context_data(self, **kwargs):
        kwargs['tot'] = NewsVerified.objects.all().count()
        kwargs['cronaca'] = NewsVerified.objects.filter(category__exact='cronaca').count()
        kwargs['sport'] = NewsVerified.objects.filter(category__exact='sport').count()
        kwargs['economia'] = NewsVerified.objects.filter(category__exact='economia').count()
        kwargs['tecnologia'] = NewsVerified.objects.filter(category__exact='tecnologia/scienze').count()
        # percentuali notizie vere e false
        kwargs['vere'] = NewsVerified.objects.filter(status__exact=1).count() / kwargs['tot'] * 100
        kwargs['false'] = NewsVerified.objects.filter(status__exact=0).count() / kwargs['tot'] * 100

        return super().get_context_data(**kwargs)
