from django.test import TestCase

from report.models import RequestedInfo, NewsReported, Donation


class StatisticsTest(TestCase):
    def setUp(self):
        self.info = RequestedInfo.objects.create(mail="indirizzo@mail.com", text="Questo è il testo")
        self.news = NewsReported.objects.create(object="oggetto della segnalazione", link="www.aaa.com")
        self.donation = Donation.objects.create(name="Marco", surname="Lupis", amount=18)

    def test_info_model(self):
        self.assertTrue(isinstance(self.info, RequestedInfo))
        lunghezza_max_mail = self.info._meta.get_field('mail').max_length
        self.assertEqual(lunghezza_max_mail, 30)
        self.assertEqual(self.info.__str__(), self.info.text)

    def test_newsreported_model(self):
        self.assertTrue(isinstance(self.news, NewsReported))
        lunghezza_max_object = self.news._meta.get_field('object').max_length
        lunghezza_max_link = self.news._meta.get_field('link').max_length
        self.assertEqual(lunghezza_max_object, 70)
        self.assertEqual(lunghezza_max_link, 250)
        self.assertEqual(self.news.__str__(), self.news.object)

    def test_donation_model(self):
        self.assertTrue(isinstance(self.donation, Donation))
        lunghezza_max_name = self.donation._meta.get_field('name').max_length
        lunghezza_max_surname = self.donation._meta.get_field('surname').max_length
        self.assertEqual(lunghezza_max_name, 30)
        self.assertEqual(lunghezza_max_surname, 30)
        self.assertGreaterEqual(self.donation.amount, 0)

    def tearDown(self):
        del self.info, self.news, self.donation
