from django.db import models


class RequestedInfo (models.Model):
    mail = models.EmailField(max_length=30)
    text = models.TextField()

    def __str__(self):
        return f"{self.text}"


class NewsReported (models.Model):
    object = models.CharField(max_length=70)
    link = models.URLField(max_length=250)

    class Meta:
        verbose_name_plural = 'NewsReported'

    def __str__(self):
        return f"{self.object}"


class Donation (models.Model):
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    amount = models.PositiveIntegerField()
