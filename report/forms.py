from crispy_forms.layout import Submit
from django import forms
from report.models import RequestedInfo, NewsReported, Donation
from crispy_forms.helper import FormHelper


class ReportForm (forms.ModelForm):

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.add_input(Submit('report_submit', 'Invia segnalazione'))
    helper.inputs[0].field_classes = 'btn btn-danger'

    class Meta:
        model = NewsReported
        fields = ('object', 'link')


class InfoForm (forms.ModelForm):

    helper = FormHelper()
    helper.form_id = 'info_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('info_submit', 'Invia richiesta'))
    helper.inputs[0].field_classes = 'btn btn-danger'

    class Meta:
        model = RequestedInfo
        fields = ('mail', 'text')


class DonationForm (forms.ModelForm):

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Invia donazione'))
    helper.inputs[0].field_classes = 'btn btn-danger'

    class Meta:
        model = Donation
        fields = ('name', 'surname', 'amount')
