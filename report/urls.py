from django.urls import path
from report.views import NewsAndInfoCreate, DonationCreate, ReadNews, SearchResults, Statistics

app_name = 'report'

urlpatterns = [
    path('report-news/', NewsAndInfoCreate.as_view(), name='report-create'),
    path('donate/', DonationCreate.as_view(), name='donation-create'),
    path('read_news/<int:pk>', ReadNews.as_view(), name='read-news'),
    path('results/', SearchResults.as_view(), name='results'),
    path('statistics/', Statistics.as_view(), name='statistics')
]
