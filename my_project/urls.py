"""my_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from my_project.views import SignUpView, FormCompiled, NewsList
from django.contrib.auth import views as auth_views  # per evitare conflitti con le mie views

urlpatterns = [
    path('', NewsList.as_view(), name='home'),
    path('admin/', admin.site.urls),
    path('report/', include('report.urls')),
    path('verify/', include('verify.urls')),
    path('user_create/', SignUpView.as_view(), name='user-create'),
    path('form_compiled', FormCompiled.as_view(), name='form-compiled'),
    path('login', auth_views.LoginView.as_view(), name='login'),
    path('logout', auth_views.LogoutView.as_view(), name='logout'),
]
