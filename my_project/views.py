from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, ListView

from verify.models import NewsVerified


class SignUpView(CreateView):

    form_class = UserCreationForm
    template_name = 'registration/user_create.html'
    success_url = reverse_lazy('form_compiled')


class FormCompiled(TemplateView):

    template_name = 'form_compiled.html'


class NewsList(ListView):
    model = NewsVerified
    template_name = 'home.html'

    def get_context_data(self, **kwargs):  # visualizzo soltanto le notizie approvate, in ordine di approvazione
        kwargs['object_list'] = NewsVerified.objects.filter(approved__exact='ok').order_by('-approved')
        return super(NewsList, self).get_context_data(**kwargs)
