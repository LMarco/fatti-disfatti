# Fatti disfatti

## Requisiti
Per lo sviluppo del progetto sono stati utilizzati:  
Python version 3.9.5  
Django version 4.0.1  
pipenv version 2021.5.29  
django-crispy-forms

## Come avviare l'applicazione

Per eseguire l'applicazione, aprire la cartella del progetto ed avviare il virtual environment:  
```pipenv shell```  

Dopodichè possiamo far partire il server:  
```python manage.py runserver```  

e,da un browser, collegarsi a http://127.0.0.1:8000/

## Test

Per i test basterà lanciare il comando  
```python manage.py test report```    
per i test dell' app report, e  
```python manage.py test verify```  
per i test dell'app verify

## Utenti

È possibile utilizzare i seguenti dati per effettuare il login di utenti già esistenti:  

utente **verificatore**  
*username*: marco2  
*password*: prova2prova2  

utente **amministratore**  
*username*: marco  
*password*: ocram  




